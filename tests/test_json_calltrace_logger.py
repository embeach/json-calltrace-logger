# Sample Test passing with nose and pytest
import os
import traceback
import pytest#type:ignore
import json_calltrace_logger #to use activateDebugLogger, deactivateDebugLogger, set_logging_allowed, get_logging_allowed
from json_calltrace_logger import done, goal, stat, compact  # level: 10 is default debug. 20 is default info

DEBUG = 10


def _testfunc():
    var1 = 1
    var2 = 2
    goal(DEBUG, 'some tests with unicode utf8: äöü€@ßj', var1=var1, var2=var2,debug=DEBUG)
    var1 += 1
    stat(DEBUG, 'status_vars', var1=var1)
    var2 += 2
    stat(DEBUG, 'status_vars with compact 1',compact_vars=compact('var1','var2','DEBUG'))
    stat(DEBUG, 'status_vars with compact 2',compact('var1','var2'))#this is wrong use of compact
    done(DEBUG, 'some tests', var1=var1, var2=var2)
    


def test_debug_report1():
    reportName='report1'
    baseDir=os.path.dirname(os.path.realpath(__file__))
    json_calltrace_logger.activateDebugLogger(baseDir,reportName)
    _testfunc()
    json_calltrace_logger.deactivateDebugLogger()
    with open(f'{os.path.dirname(os.path.realpath(__file__))}/logs/{reportName}.json', 'r') as file:
        lines=file.readlines()
    assert len(lines)>=18 and len(lines)<=88
def test_debug_report2():
    reportName='report2'
    json_calltrace_logger.activateDebugLogger(os.path.dirname(os.path.realpath(__file__)),reportName)
    def modulFunc():
        done(11,'from modulFunc')
        done(11,'2from modulFunc')
    class outerClass:
        def method(self):
            self.hallo='hallo'
            modulFunc()
            done(10,'from outerClass.method',var1=['num1',{'halloKey':(1,2,3)}])
            def innerMethod(self):
                done(10,'from innerMethod',hallo=self.hallo)
                tb=traceback.extract_stack()
                for frame in tb:
                    #print frame
                    pass
            innerMethod(self)
    done(10,'from modul')
    goal(11,'from modul')
    t=outerClass()
    t.method()
    stat(11,'from modul',t=t.__dict__)
    for i in range(10):
        stat(11,u'from moduläöüß@')
    modulFunc()
    json_calltrace_logger.deactivateDebugLogger()
    with open(f'{os.path.dirname(os.path.realpath(__file__))}/logs/{reportName}.json', 'r') as file:
        lines=file.readlines()
    print('lines:'+str(len(lines)))
    assert len(lines)>=52 and len(lines)<=122
def test_debug_report_fileNotFound():
    reportName='report'
    baseDir=os.path.dirname(os.path.realpath(__file__))+'/nowhere'
    #with pytest.raises(FileNotFoundError) as e_info:
    try:
        json_calltrace_logger.activateDebugLogger(baseDir,reportName)
    except FileNotFoundError:
        assert True
def test_debug_report_deactivateWithoutActivate():
    #with pytest.raises(AssertionError) as e_info:
    try:
        json_calltrace_logger.deactivateDebugLogger()
    except Exception as e:
        assert type(e).__name__=='Exception'
        assert str(e) =='debugLoggger needs to activated before closable'
def test_without_logging_allowed():
    assert json_calltrace_logger.get_logging_allowed()==True
    json_calltrace_logger.set_logging_allowed(False)
    assert json_calltrace_logger.get_logging_allowed()==False
    done(10,'from modul')
    goal(11,'from modul')
    json_calltrace_logger.set_logging_allowed(True)
    assert json_calltrace_logger.get_logging_allowed()==True
if __name__ == '__main__':
    test_debug_report1()
    test_debug_report2()
    test_debug_report_fileNotFound()
    test_debug_report_deactivateWithoutActivate()
    test_without_logging_allowed()
