import io
import os
import re

from setuptools import find_packages
from setuptools import setup


def read(filename):
    filename = os.path.join(os.path.dirname(__file__), filename)
    text_type = type(u"")
    with io.open(filename, mode="r", encoding='utf-8') as fd:
        return re.sub(text_type(r':[a-z]+:`~?(.*?)`'), text_type(r'``\1``'), fd.read())


setup(
    name="json-calltrace-logger",
    version="0.1.3",
    url="https://test.com/test/json-calltrace-logger",
    license='MIT',

    author="embeach",
    author_email="emb_gitl+gitl@mailbox.org",

    description="A logger to trace execution and variables by producing a JSON call-tree with embedded log entries - useful for debugging - robust against fatal failures.",
    long_description=read("README.md"),

    packages=['json_calltrace_logger'],  # i.e., the root package
    package_data={'json_calltrace_logger': ['LICENSE', 'py.typed']},
    include_package_data=True,

    test_suite='tests',

    install_requires=[],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.7',
    ],
)
