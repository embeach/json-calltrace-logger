# -*- coding: UTF-8 -*-
#public api: activateDebugLogger(),deactivateDebugLogger(),goal(),done(),stat(),compact()
#from __future__ import annotations #not used, instead "string typing" is used for forward refs, because eclipse pydev doesn't support it yet
from typing import Dict, Optional, Any, Union
import logging
import os
import re
import traceback #use extract_stack
import json

import string
import random
import datetime
import inspect

_logging_allowed=True
_debugLogger:Optional['DebugLogger']=None#may be in future one logger per threat with dict (threatId,threat)
_debugLogger_Handler:Optional['DebugReport_Handler']=None
def set_logging_allowed(logging_allowed:bool)->None:
    global _logging_allowed
    _logging_allowed=logging_allowed
def get_logging_allowed()->bool:
    return _logging_allowed
def compact(*names:str)->Dict[str,Any]:
    '''
    assumes string list as variables, retrieves values by introspection and returns json as string
    example:
    >var1=1
    >var2=2
    >compact('var1','var2')
    <{'var1'=1,'var2'=2}
    '''
    caller = inspect.stack()[1][0]  # caller of compact()
    _vars = {}
    for n in names:
        if n in caller.f_locals:
            _vars[n] = caller.f_locals[n]
        elif n in caller.f_globals:
            _vars[n] = caller.f_globals[n]
    return _vars



'''
Idea: one Logger can have multiple Handler. But for DebugLogger there seems to be only one handler reasonable. This is the DebugReport_handler (may be better DebugLoggerHandler?)
'''
class DebugLogger(logging.Logger):  # old: DebugLogJson
    def __init__(self, loggerName:str, level:Union[int,str]=logging.NOTSET)->None:
        logging.Logger.__init__(self, loggerName, level)
        self.config = {  #TODO check if there exists a config var in parents
            'maxHeadingLength':40
        }



    #normally only DebugReport_Handler is added, but may be also other handlers will work (i.e. for cmd output)
    def addHandler(self, hdlr:Optional[logging.Handler]=None,baseDir='',filename='report',useSubDir=True):
        #print(compact('baseDir'))
        global _debugLogger_Handler
        if hdlr is None:
            hdlr = DebugReport_Handler(baseDir,filename,useSubDir)
            _debugLogger_Handler = hdlr
        logging.Logger.addHandler(self, hdlr)

    def log(self, level, msg, *args, **kwargs):
        if len(msg) > self.config['maxHeadingLength']:
            heading = msg[:self.config['maxHeadingLength'] - 1]
            kwargs['txt'] = msg
        else:
            heading = msg
        kwargs['mode'] = 'stat'
        _incrementSkip(kwargs)
        self._rpt(level, heading, *args, **kwargs)

    def _rpt(self, level, heading, *args, **kwargs):#heading is allowed to be empty str
        rawCallStack = traceback.extract_stack()  # format: [(filename, line number, function name, text),...]
        _incrementSkip(kwargs)
        assert (len(rawCallStack) > kwargs['skip'])
        pCallStack = self._prepareCallStack(rawCallStack[:-kwargs['skip']])
        del kwargs['skip']
        userExtra = kwargs.copy()
        del userExtra['mode']
        kwargs['pCallStack'] = pCallStack
        kwargs['heading'] = heading
        kwargs['userExtra'] = userExtra
        if self.isEnabledFor(level):
            self._log(level, '', args, extra=kwargs)

    def _prepareCallStack(self, rawCallStack):
        # expected frame format: (file(where is the caller defined),lineno in file(where is the call),funcname(who calls),text (what is the corresponding call-statement-string in lineno)
        preparedCallStack = []
        lastFrame = ('init', 0, 'init', 'init')
        for i in range(len(rawCallStack)):
            preparedCallStack.append({'callTarget':(str(rawCallStack[i][0]), str(rawCallStack[i][2])), 'callDeclaration':lastFrame})
            lastFrame = [k for k in rawCallStack[i]]
        return preparedCallStack + [{'callDeclaration':lastFrame}]


class DebugReport_Handler(logging.Handler):

    def setFilenamePostfix(self, fileNamePostfix=''):
        self.config['fileNamePostfix'] = fileNamePostfix

    def __init__(self, baseDir='',filename='',useSubDir=True):#default filename=='report'
        logging.Handler.__init__(self)
        self.set_name(self.__class__.__name__+filename)
        self.config = {
            'baseDir' : '',
            'useSubDir':useSubDir,
            'subDir' :'logs',
            'fileNamePrefix' :'report',
            'fileNamePostfix' :'',
            'fileExtension':'.json',
            'appendFilePostfix':'_end'
        }
        self.config['baseDir']=baseDir if baseDir!='' else os.path.dirname(os.path.realpath(__file__))
        self.config['fileNamePrefix']=filename if filename!='' else self.config['fileNamePrefix']
        self.jsonConstructor = DebugReport_JsonConstructor()
        self.isActiveFileHandlers = False
        self._activateFileHandlers()

    def _activateFileHandlers(self):
        self.filepathIncNameWithoutExtension = self.config['baseDir'] + (('/' + str(self.config['subDir'])) if  self.config['useSubDir']else'')+ '/' + str(self.config['fileNamePrefix']) + str(self.config['fileNamePostfix'])
        mainFile_Handler_filepathIncName=self.filepathIncNameWithoutExtension + self.config['fileExtension']
        print("debug_report: going to write in:"+mainFile_Handler_filepathIncName)
        try:
            self.mainFile_Handler = logging.FileHandler(mainFile_Handler_filepathIncName, 'w')
            self.mainFile_Handler.set_name(self.mainFile_Handler.__class__.__name__+self.config['fileNamePrefix'])
            appendFile_Handler_filepathIncName=self.filepathIncNameWithoutExtension + self.config['appendFilePostfix'] + self.config['fileExtension']
            self.appendFile_Handler = logging.FileHandler(appendFile_Handler_filepathIncName, 'w')
            self.appendFile_Handler.set_name(self.appendFile_Handler.__class__.__name__+self.config['fileNamePrefix']+self.config['appendFilePostfix'])
            self._lastAppendStr = ''
            self.isActiveFileHandlers = True
        except FileNotFoundError:
            raise FileNotFoundError('Json File has to be created first:'+mainFile_Handler_filepathIncName)

    def handle(self, record):
        if self.filter(record):
            (mainStr, appendStr) = self.jsonConstructor.insertLogRecord(record)
            self._lastAppendStr = appendStr
            self.emit(mainStr, appendStr)

    def emit(self, mainStr, appendStr):
        assert (self.isActiveFileHandlers)
        # print  mainStr
        assert (self.mainFile_Handler.stream is not None)
        self.mainFile_Handler.handle(logging.makeLogRecord({'msg':mainStr}))
        assert (self.appendFile_Handler.stream is not None)
        self.appendFile_Handler.handle(logging.makeLogRecord({'msg':appendStr}))

    def beforeClose(self):
        assert (self.isActiveFileHandlers)
        self.mainFile_Handler.handle(logging.makeLogRecord({'msg':self._lastAppendStr}))

    def close(self):
        self.isActiveFileHandlers = False
        self.mainFile_Handler.close()
        self.appendFile_Handler.close()
        logging.Handler.close(self)

#TODO test
def _getFileName(filePath):
    name = 'init'
    if filePath != name:
        pattern = '\\\\([^\\\\]*?)$'
        mo = re.search(pattern, filePath)
        if mo:
            name = mo.group(1)
        else:
            name = filePath
    return name


class DebugReport_JsonConstructor:
    """
    This Class has the following Concepts:
    FunctionNodes (FunctionNodes): are all Nodes that references a Function (FunctinNodes define the TreeStructure where ContentNodes can be inserted)
    LogContentNodes (ContentNodes): are Nodes that represent a DebugLogCall (there are currently 3 types: done,goal,stat)
    OtherNodes: are all Nodes that represent not functionNodes nor contentNodes for example things like Values (Values can also be nested)
        FuncContentNodes: are nodes that are nodes that describe the function or there frame or are associated with that.
    The CoreFeature: of JsonWriter is to write a correct Json-File with the relevant debug-infos the developer wants to have in his DebugLog.json. 
        But due to the fact, that this should also hold if errors occur, the JsonTree is splited in two files: the MainFile and the AppendFile.
        if no error occurs the whole json will be in MainFile and AppendFile is empty. if fatal error occur, MainFile has to be manually appended by AppendFile to get the correct Json.
        For that, the developer can call the report function with optional relevant variables and texts as arguments (and a priority as guard) to define a ContentNode.
        
    """

    def __init__(self):  # TODO private public notation should be uniformed..
        self.nodeGenerator = DebugReport_NodeGenerator()
        self.openBracesCounter = 0
        self._logCounter = 0
        self.lastInputPath = None
        self.outMain = ''
        self.komma = ","

    def insertLogRecord(self, record):

        def _resetOut(self):
            self.outMain = ''

        def _prepareAndinsertContentNode(self, newInputPath, contentNode):

            def _prepareForNewContentNode(self, newInputPath):

                def _insertRootNode(self, newInputPath):
                    self._add("{")
                    self.openBracesCounter += 1
                    self._addLineBreak()
                    self._insertContentNode(self.nodeGenerator.getRootContentNode())
                    self.lastInputPath = []  # newInputPath[0]]

                def _comparePathes(self, newInputPath, lastInputPath):  # assume root is first element
                    newInputPathLen = len(newInputPath)
                    equalPath = []
                    extensionPath = []
                    maxEqual = None
                    for i in range(len(lastInputPath)):
                        if i < newInputPathLen and lastInputPath[i] == newInputPath[i]:
                            equalPath.append(lastInputPath[i])
                            maxEqual = i
                        else:
                            break
                    if maxEqual is not None:
                        if maxEqual + 1 < newInputPathLen:
                            extensionPath = newInputPath[maxEqual + 1:]
                    else:
                        extensionPath = newInputPath
                    return (equalPath, extensionPath)

                def _closeFunctionNodes(self, nb):
                    self._add(self._getClosingBraces(nb))
                    self.openBracesCounter -= nb

                def _openFunctionNodes(self, extensionPath):
                    for frame in extensionPath:
                        self._addKommaSep()
                        self._addLineBreak()
                        self._add(json.dumps(self.nodeGenerator.getFunctionNodeKey(frame) + self._appendJsonConstrDebugInfos(False), ensure_ascii=False) + ":{")
                        self.openBracesCounter += 1
                        self._addLineBreak()
                        self._insertContentNode(self.nodeGenerator.getFuncContentNode(frame))

                if self.lastInputPath is None:
                    _insertRootNode(self, newInputPath)
                (equalPath, extensionPath) = _comparePathes(self, newInputPath, self.lastInputPath)
                _closeFunctionNodes(self, len(self.lastInputPath) - len(equalPath))
                _openFunctionNodes(self, extensionPath)
                self.lastInputPath = newInputPath

            _prepareForNewContentNode(self, newInputPath)
            self._addKommaSep()
            self._addLineBreak()
            self._insertContentNode(contentNode)

        def _out(self):
            return (self.outMain, self._getClosingBraces(self.openBracesCounter))

        self._logCounter += 1
        contentNode = self.nodeGenerator.getLogContentNode(record)
        _resetOut(self)
        _prepareAndinsertContentNode(self, record.pCallStack[:-1], contentNode)
        return _out(self)

    def _insertContentNode(self, contentNode):
        self._add(json.dumps(contentNode[0] + self._appendJsonConstrDebugInfos(False), ensure_ascii=False) + ':' + json.dumps(contentNode[1], ensure_ascii=False))

    def _appendJsonConstrDebugInfos(self, isActive=True):
        return '>l:' + str(self._logCounter) + '>brs:' + str(self.openBracesCounter) if isActive else ''

    def _add(self, string):
        self.outMain += string

    def _getClosingBraces(self, nb):
        return '}' * nb

    def _addKommaSep(self):
        self._add(',')

    def _addLineBreak(self):
        self._add("\n")


class DebugReport_NodeGenerator:

    def __init__(self):
        logContentFmt = '%(mode)s>%(heading)s>line:%(lineno)d>time:%(asctime)s'
        self.logContentFormatter = logging.Formatter(logContentFmt)

    def getLogContentNode(self, record):
        frame = record.pCallStack[-1]
        record.lineno = frame['callDeclaration'][1]
        heading = self.logContentFormatter.format(record) + '>r:' + self._getRandomStr()
        nodeContent = record.userExtra
        contentNode = (heading, nodeContent)
        return contentNode

    def getFuncContentNode(self, frame):
        return ('info>calledAtLine:' + str(frame['callDeclaration'][1]) + '>txt:' + str(frame['callDeclaration'][3]) + '>in:' + _getFileName(frame['callDeclaration'][0]), frame['callDeclaration'][0])  # callDecl

    def getRootContentNode(self):

        def _getMicTimeStr(extended=True):
            _format = '%Y-%m-%d %H:%M:%S' if extended else '%H:%M:%S'
            return datetime.datetime.fromtimestamp(logging._startTime).strftime(_format) + ',' + str(logging._startTime).split('.')[1]  # +frac

        return ('DebugReportSessionInfo', "" + str(_getMicTimeStr()))

    def getFunctionNodeKey(self, frame):  # pCallStackFame
        return 'calls(Target):' + _getFileName(frame['callTarget'][0]) + '>' + frame['callTarget'][1] + '>r:' + self._getRandomStr()

    def _getRandomStr(self):
        _list = [str(random.choice(string.ascii_letters + string.digits)) for i in range(3)]
        return ''.join(_list)


def done(level, heading='', *args, **kwargs):
    kwargs['mode'] = 'done'
    _incrementSkip(kwargs)
    rpt(level, heading, *args, **kwargs)


def goal(level, heading='', *args, **kwargs):
    kwargs['mode'] = 'goal'
    _incrementSkip(kwargs)
    rpt(level, heading, *args, **kwargs)


def stat(level, heading='', *args, **kwargs):
    kwargs['mode'] = 'stat'
    _incrementSkip(kwargs)
    rpt(level, heading, *args, **kwargs)


def rpt(level, heading='', *args, **kwargs):
    #global _debugLogger
    if get_logging_allowed():
        activateDebugLogger()
        assert ('mode' in kwargs)
        _incrementSkip(kwargs)
        _debugLogger._rpt(level, heading, *args, **kwargs)


def _incrementSkip(kwargs):
    kwargs['skip'] = kwargs['skip'] + 1 if 'skip' in kwargs else 1





#TODO documentation especially baseDir,filename
def activateDebugLogger(baseDir='',filename='',useSubDir=True):
    '''
    creates a DebugLogger with one DebugReport_Handler with two FileHandlers. 
    The Logs are written in baseDir/logs/filename[Postfix].json andbaseDir/logs/filename[Postfix]_end.json.
    The second file is only needed if an  fatal error occurs - then the json object from the first file can be completed by appending the last line of the second (.._end) file.
    The library will not create the logs dictionary. This means it has to be created manually.
    A common example call would be the following (it will assume the dir of the current python file as baseDir):
    >from debug_report import debug_report_m
    >from debug_report.debug_report_m import done, goal, stat, compact
    >debug_report_m.activateDebugLogger(os.path.dirname(os.path.realpath(__file__)),'report1')
    >#then anywhere in the code for example
    >goal(10,'risky operation',compact('var1','var2'))
    >done(10,'risky operation',var1=var1)
    >debug_report.deactivateDebugLogger
    The library is not ready to cope with a multi-thread scenario.
    '''
    global _debugLogger
    if _debugLogger==None:
        logging.setLoggerClass(DebugLogger)#seems to be configuration step
        _debugLogger = logging.getLogger('DebugLoggerTestLogger')
        _debugLogger.addHandler(baseDir=baseDir,filename=filename,useSubDir=useSubDir)
        _debugLogger.setLevel(1)
        print('debugLogger is now active')


def deactivateDebugLogger():
    '''
    shutdown DebugLogger and Handlers. See activateDebugLogger for examples.
    '''
    global _debugLogger, _debugLogger_Handler
    if _debugLogger is None or _debugLogger_Handler is None:
        raise Exception('debugLoggger needs to activated before closable')
    _debugLogger.removeHandler(_debugLogger_Handler)
    _debugLogger_Handler.beforeClose()
    _debugLogger_Handler.close()
    _debugLogger=None
    _debugLogger_Handler=None
    print('debugLogger is now closed and deactivated')


