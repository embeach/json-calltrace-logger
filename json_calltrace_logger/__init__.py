"""A python logger to trace execution and variables by producing a focused JSON call-tree with embedded log entries."""
from json_calltrace_logger.report import activateDebugLogger, deactivateDebugLogger, set_logging_allowed, get_logging_allowed, done, goal, stat, compact
__author__ = 'embeach'

