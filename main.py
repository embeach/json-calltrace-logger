'''
Created on Mar 25, 2020

@author: embeach

example usage demo
dir ./logs has to be created manually
'''
import os
from json_calltrace_logger import report #to use activateDebugLogger, deactivateDebugLogger
from json_calltrace_logger import done, goal, stat, compact
DEBUG = 10
def _foo():
    var1 = 1
    var2 = 2
    goal(DEBUG, 'some goal', var1=var1, var2=var2,debug=DEBUG)
    var1 += 1
    stat(DEBUG, 'state_vars', var1=var1)
    var3 = [1,2,3]
    stat(DEBUG, 'state_vars with compact 1',compact_vars=compact('var1','var2','var3','DEBUG'))
    stat(DEBUG, 'state_vars with compact 2',compact('var1','var2'))#this is wrong use of compact!!
    done(DEBUG, 'something is done', var1=var1, var2=var2)
    
def main():
    reportName='report1'
    baseDir=os.path.dirname(os.path.realpath(__file__))
    report.activateDebugLogger(baseDir,reportName)
    _foo()
    report.deactivateDebugLogger()

main()
