JSON call-trace logger
====================== 
A python logger to trace execution and variables by producing a focused JSON call-tree with embedded log entries. It is useful for debugging and code analysis and is robust against fatal failures.  
Note: This is a highly experimental project without extensive test coverage. Do not rely on this!  
Note: There might be issues especially if multiple threats are involved.
###### Names:
pip: json-calltrace-logger  
python: json_calltrace_logger

Usage Example
-------------
```python
#main.py
import os
from json_calltrace_logger import report #to use activateDebugLogger, deactivateDebugLogger
from json_calltrace_logger import done, goal, stat, compact
DEBUG = 10
def _foo():
    var1 = 1
    var2 = 2
    goal(DEBUG, 'some goal', var1=var1, var2=var2,debug=DEBUG)
    var1 += 1
    stat(DEBUG, 'state_vars', var1=var1)
    var3 = [1,2,3]
    stat(DEBUG, 'state_vars with compact 1',compact_vars=compact('var1','var2','var3','DEBUG'))
    stat(DEBUG, 'state_vars with compact 2',compact('var1','var2'))#this is wrong use of compact!!
    done(DEBUG, 'something is done', var1=var1, var2=var2)
    
def main():
    reportName='report1'
    baseDir=os.path.dirname(os.path.realpath(__file__))
    report.activateDebugLogger(baseDir,reportName)
    _foo()
    report.deactivateDebugLogger()

main()
```
The following call:

```console
user@host:~/git/json-calltrace-logger$ python main.py
debug_report: going to write in:/home/user/git/json-calltrace-logger/logs/report1.json
debugLogger is now active
debugLogger is now closed and deactivated
```
will write the following log file:

```
#logs/report1.json
{
"DebugReportSessionInfo":"2020-03-25 15:36:00,2786036",
"calls(Target):main.py><module>>r:UMg":{
"info>calledAtLine:0>txt:init>in:init":"init",
"calls(Target):main.py>main>r:i4e":{
"info>calledAtLine:28>txt:main()>in:main.py":"main.py",
"calls(Target):main.py>_foo>r:iE8":{
"info>calledAtLine:25>txt:_foo()>in:main.py":"main.py",
"goal>some goal>line:13>time:2020-03-25 15:36:00,291>r:dNg":{"var1": 1, "var2": 2, "debug": 10}
,
"stat>state_vars>line:15>time:2020-03-25 15:36:00,292>r:jWn":{"var1": 2}
,
"stat>state_vars with compact 1>line:17>time:2020-03-25 15:36:00,296>r:Fam":{"compact_vars": {"var1": 2, "var2": 2, "var3": [1, 2, 3], "DEBUG": 10}}
,
"stat>state_vars with compact 2>line:18>time:2020-03-25 15:36:00,297>r:0ZZ":{}
,
"done>something is done>line:19>time:2020-03-25 15:36:00,297>r:K1u":{"var1": 2, "var2": 2}
}}}}
```
Note: `logs` dir has to be created manually - otherwise an exception will occur.  
Note: if a fatal failure occur, the `report1_end.json` file might be useful to repair the JSON with closing brackets.  
(Note: path `~` resolves to `/home/user` on Linux where `user` is the username.)

Installation
------------
This pip package is currently not available at [pypi.org](https://pypi.org/). Nevertheless it is installable in the following ways (examples):  
Suppose we have a local project A with `requirement.txt` in dir `~/some_path1`. We can either add a local folder or a git repository.  
Note: Examples work on Linux. On Windows the steps should be similar.

```console
user@host:~$ cd some_path2
user@host:~/some_path2$ git clone https://gitlab.com/embeach/json-calltrace-logger.git
```
###### 1. local folder

In requirement.txt from project A add the line `/home/user/some_path2/json-calltrace-logger`.
###### 2. git repository

In `requirement.txt` from project A add the line `git+https://gitlab.com/embeach/json-calltrace-logger.git@master#egg=json-calltrace-logger`.

----
Now dependencies for project A can be installed with:

```console
user@host:~/some_path1$ pip install -r requirements.txt
```
Note: Depending on installation the name `pip3` is needed.

Requirements
------------
* python 3
* pip

Setup python environment with miniconda3
----------------------------------------
It is possible to setup a virtual python3 environment **without admin access** with miniconda3.  
On Linux,the makefile can be used for this:  
Note: Miniconda is a package/dependency manager which uses a base environment (which should be kept clean) and is able to create arbitrary more different python environments.  
Note: miniconda3 will be installed in `~/miniconda3`  
Note: in `.bashrc` the path to miniconda3 will be added to be available.  
Note: miniconda and pip are both package manager (this means:  It is possible to use first conda install and then pip install but as soon as pip is used conda should not be used any longer for package management tasks and pip should not be used to manage conda packages.)  
Note: you can also look in make-helper.sh and run the commands manually.  
Note: in make-helper.sh the miniconda3 file is downloaded over https but checksum and signature will not be extra verified.

```console
user@host:~/some_path2/json-calltrace-logger$ make download-and-install-and-init-miniconda3
```
To create and use python/conda environments see next section.

Use tox for testing etc.
------------------------
By using [tox](https://tox.readthedocs.io/en/latest/) automation we don't need to install the requirements.txt manually. Instead tox will create and manage the needed virtual environments automatically.
##### high level:
1. create a python environment with pip/pip3
2. activate this environment. This can be verified by running `which python`.
2. install tox with `pip install tox`
3. run the tox automation process by `cd ~/some_path2` and `tox`

##### step by step with conda
Note: in the following it is assumed to have conda installed with the above makefile miniconda3 installation and use bash shell.  
Note: alternatively the makefile can be used - skip following steps 1-3 and look at **use makefile**):
###### 1 create conda environment with pip
```console
user@host:~/some_path2/json-calltrace-logger$ conda create -p ./.cenv python=3.7 pip
```
Note: currently tox is not available in conda repository - so it has to be installed with pip.  
Note: to use conda manually (without makefile) after the `make download-and-install-and-init-miniconda3` command the bash shell has to be restarted (for the .bashrc changes to take effect). The makefile commands will not need this restart.
###### 2. activate conda environment
```console
user@host:~/some_path2/json-calltrace-logger$ conda activate ./.cenv
```
###### 3. install tox
```console
user@host:~/some_path2/json-calltrace-logger$ pip install tox
```
###### 4. run tox
```console
user@host:~/some_path2/json-calltrace-logger$ tox
```
###### use makefile
Use the Makefile to "simplify" above steps:

```console
user@host:~/some_path2/json-calltrace-logger$ make create-conda-env
user@host:~/some_path2/json-calltrace-logger$ make activate-conda-env-and-run-tox
```
Note: in bash autocomplete with tab is available.

Compatibility
-------------
No known issues.

Licence
-------
Free and Open Source MIT License.

Authors
-------

`json-calltrace-logger` was written by embeach ([gitlab](https://gitlab.com/embeach),[mail](mailto:emb_gitl+jcl@mailbox.org)) 2015-2020.  
History Note:
Code was developed in 2015 as a private project. In 2020 the project was reused and funding for porting and maintaining was supported by TH-Lübeck([th-luebeck.de](https://th-luebeck.de/en))/Schleswig-Holstein.

Contributer License Agreement
------------
**By contributing** to this project (namely `json-calltrace-looger`) contributer assure that they hold all necessary copyrights/Urheberrechte on there contributions and **accept** to license there contributions under the free and open-source **MIT license**. Such that the community can use there contributions with all rights/freedoms that the MIT license grants. If contributers does either not have the copyrights/Urheberrechte to there contributions or does not accept the MIT license, they are not allowed to contribute copyright protected material to this repository.  
Furthermore **by contributing** (especially with every commit) contributer **grant every user/licensee** the right to **re-license** the project or any parts of it under every **[OSI](https://opensource.org/licenses/)-approved License**, without prior consent. This is to achieve the maximal compatibility with other licenses to avoid future issues.  
Note: Contributers are assumed to have **read and accept this contract**.  
Attribution: Contributer are welcome to indicate there authorship/copyright-ownership/Urheberschaft by adding there preferred name/pseudonym to any own contributed parts (as long as the name is not ambiguous with already listed contributers) and adding credits in the authors section of this file. Furthermore they are welcome to add there preferred name/pseudonym to the MIT license file `LICENSE` (as separate line with date) if there contributions are rather substantial.  
Note: The License/Copyright-Notices might slightly change in the future to comply with [reuse.software](https://reuse.software/)