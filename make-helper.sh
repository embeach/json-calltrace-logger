#!/bin/bash
#make-helper.sh for Linux only! No warranty!
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
MINICONDA_ROOT=$HOME/miniconda3
if [ $1 = 'download-and-install-and-init-miniconda3' ]
then
	mkdir -p $HOME/Downloads
	cd $HOME/Downloads
	#download miniconda3
	echo 'wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh..'
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	#install miniconda3 in $HOME/miniconda3 with default settings
	echo 'bash $HOME/Downloads/Miniconda3-latest-Linux-x86_64.sh -b -p '$MINICONDA_ROOT
	bash $HOME/Downloads/Miniconda3-latest-Linux-x86_64.sh -b -p $MINICONDA_ROOT
	#initialize miniconda3 - will add an export-conda-path-command to .bashrc and somehow seems to be usable to activate environments
	echo $MINICONDA_ROOT'/bin/conda init..'
	$MINICONDA_ROOT/bin/conda init
	#after init shell has to be restarted for changes in .bashrc to take effect.. but we will ignore this and work with  full path $MINICONDA_ROOT
elif [ $1 = 'create-conda-env' ]
then
	#the hook seems to be needed to prevent warning of missing conda init
	eval "$($MINICONDA_ROOT/bin/conda shell.bash hook)"
	#with conda create virtual python environment (in project dir) with python 3.7 pip (tox has to be installed later because it isnt available in conda repo)
	(echo y) | conda create -p ./.cenv python=3.7 pip
	#use/activate the specified python environment
	$MINICONDA_ROOT/bin/conda activate ./.cenv/
	pip install tox
elif [ $1 = 'activate-conda-env-and-run-tox' ]
then
	eval "$($MINICONDA_ROOT/bin/conda shell.bash hook)"
	#use/activate the specified python environment
	$MINICONDA_ROOT/bin/conda activate ./.cenv/
	#will output the path to the used/activated python environment/executables
	which python
	#run tox - this will use .tox to create required new python environments and download and install required pip packages there
	tox
fi
