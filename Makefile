# Makefile for Linux only! No warranty!
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# For details see README.md and make-helper.sh
download-and-install-and-init-miniconda3:
	bash make-helper.sh 'download-and-install-and-init-miniconda3'
# create conda environment with pip and pip based tox install.
create-conda-env:
	bash make-helper.sh 'create-conda-env'
activate-conda-env-and-run-tox:
	bash make-helper.sh 'activate-conda-env-and-run-tox'
run: activate-conda-env-and-run-tox